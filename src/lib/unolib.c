/*
 * UnoLib.c
 *
 * Source File for Uno Library
 *
 *  Created on: Sep 7, 2015
 *      Author: Pojostick
 */

#include "unolib.h"

#include <stdlib.h>

Card new_card(Color color, Value value) {
	Card card;
	card.color = color;
	card.value = value;
	return card;
}

Deck new_deck(unsigned int size) {
	Deck deck = malloc(sizeof(*deck));
	deck->index = 0;
	deck->size = size;
	deck->cards = malloc(size * sizeof(Card));

	int card = 0;
	Color color = 0;
	Value value = 0;

	for (color = RED; color <= BLUE; color++) {
		// Add 1 zero
		deck->cards[card++] = new_card(color, ZERO);

		// Add 2 of each number card except wilds
		for (value = ONE; value <= DRAW_TWO; value++) {
			deck->cards[card++] = new_card(color, value);
			deck->cards[card++] = new_card(color, value);
		}

		// Add 1 of each wild
		for (value = WILD; value <= WILD_DRAW_FOUR; value++) {
			deck->cards[card++] = new_card(RAINBOW, value);
		}
	}

	return deck;
}

// see http://www.geeksforgeeks.org/shuffle-a-given-array/
void shuffle_deck(Deck deck, unsigned int from, unsigned int to,
		unsigned int seed) {
	srand(seed);
	int i = 0;

	for (i = to - 1; i > from; i--) {
		// select random between from inclusive and to exclusive
		int r = rand() % (i - from + 1) + from;
		Card temp = deck->cards[i];
		deck->cards[i] = deck->cards[r];
		deck->cards[r] = temp;
	}
}

Card draw_card(Deck deck) {
	if (deck->index < deck->size) {
		return deck->cards[deck->index++];
	}
	return BLANK;
}

void free_deck(Deck deck) {
	free(deck->cards);
	free(deck);
}

int diff(Card first, Card second) {
	return (((first.value - second.value) << VALUE_BITS)
			| (first.color - second.color));
}
