/*
 * gameio.c
 *
 * Source file for Game Input/Output
 *
 *  Created on: Sep 7, 2015
 *      Author: Pojostick
 */

#include "gameio.h"

#include <stdlib.h>

Hand new_hand(unsigned int size) {
	Hand hand = malloc(sizeof(hand));
	hand->size = 0;
	hand->max = size;
	hand->cards = malloc(size * sizeof(Card));
	return hand;
}

Card remove_card(Hand hand, unsigned int index) {
	if (index <= hand->size) {
		Card card = hand->cards[index];
		int i = 0;
		for (i = index; i < hand->size - 1; i++) {
			hand->cards[i] = hand->cards[i + 1];
		}
		hand->size--;
		return card;
	}
	return BLANK;
}

void add_cards(Hand hand, unsigned int count, Card cards[]) {
	int i = 0;

	if (hand->size + count > hand->max) {
		hand->max = hand->size + count;
		hand->cards = realloc(hand->cards, hand->max * sizeof(Card));
	}

	for (i = 0; i < count; i++) {
		hand->cards[hand->size + i] = cards[i];
	}
	hand->size = hand->size + count;
}

void free_hand(Hand hand) {
	free(hand->cards);
	free(hand);
}

Turnorder new_turnorders(unsigned int size) {
	Turnorder turnorder = malloc(sizeof(*turnorder));
	turnorder->size = size;
	turnorder->players = malloc(size * sizeof(Hand));
	int i = 0;

	for (i = 0; i < size; i++) {
		turnorder->players[i] = new_hand(8);
	}

	return turnorder;
}

void free_turnorders(Turnorder turnorder) {
	int i = 0;

	for (i = 0; i < turnorder->size; i++) {
		free_hand(turnorder->players[i]);
	}
	free(turnorder->players);
	free(turnorder);
}
