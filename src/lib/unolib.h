/*
 * UnoLib.h
 *
 * Interface header file for Uno Library
 *
 *  Created on: Sep 7, 2015
 *      Author: Pojostick
 */

#ifndef LIB_UNOLIB_H_
#define LIB_UNOLIB_H_

#define DECK_SIZE 108
#define VALUE_BITS 4

typedef enum {
	RED, YELLOW, GREEN, BLUE, RAINBOW, UNKNOWN_COLOR
} Color;

typedef enum {
	ZERO,
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	REVERSE,
	SKIP,
	DRAW_TWO,
	WILD,
	WILD_DRAW_FOUR,
	UNKNOWN_VALUE
} Value;

typedef struct {
	Color color;
	Value value;
} Card;

typedef struct {
	unsigned int index, size;
	Card *cards;
}*Deck;

static const Card BLANK = { .color = UNKNOWN_COLOR, .value = UNKNOWN_VALUE };

Card new_card(Color color, Value value);
Deck new_deck();
void shuffle_deck(Deck deck, unsigned int from, unsigned int to,
		unsigned int seed);
Card draw_card(Deck deck);
void free_deck(Deck deck);
int diff(Card first, Card second);

#endif /* LIB_UNOLIB_H_ */
