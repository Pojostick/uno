/*
 * Uno.c
 *
 * Main file for running Uno
 *
 *  Created on: Sep 6, 2015
 *      Author: Pojostick, Tushar
 */

#include "lib/unolib.h"
#include "lib/gameio.h"

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

void print_card(Card card) {
	printf("{ color:%d, value:%d }\n", card.color, card.value);
}

void print_all(Card *card) {
	printf("Card@%p: ", card);
	print_card(*card);
}

int equals(Card first, Card second) {
	return !diff(first, second);
}

int main(int argc, char *argv[]) {
	puts("Uno! v0.0\n=====");

	puts("Start of main method.");

	puts("Card matching test");

	Card a = new_card(RED, ONE);
	Card b = new_card(YELLOW, ONE);
	Card c = new_card(YELLOW, TWO);
	Card d = new_card(RED, TWO);
	Card e = new_card(RED, ONE);

	print_all(&a);
	print_all(&b);
	print_all(&c);
	print_all(&d);
	print_all(&e);

	printf("diff(%p, %p): %d\n", &a, &b, diff(a, b));
	printf("diff(%p, %p): %d\n", &a, &c, diff(a, c));
	printf("diff(%p, %p): %d\n", &a, &d, diff(a, d));
	printf("diff(%p, %p): %d\n", &a, &e, diff(a, e));

	puts("Deck test (new deck)");

	Deck deck = new_deck(DECK_SIZE);

	int i = 0;
	for (i = 0; i < 5; i++) {
		print_card(draw_card(deck));
	}

	puts("Shuffle test (shuffled cards 6th-108th)");

	shuffle_deck(deck, 5, 108, 0);
	deck->index = 0;

	for (i = 0; i < 10; i++) {
		print_card(draw_card(deck));
	}

	free_deck(deck);

	puts("End of main method.");

	return EXIT_SUCCESS;
}
