/*
 * gameio.h
 *
 * Interface header file for Game Input/Output
 *
 *  Created on: Sep 7, 2015
 *      Author: Pojostick
 */

#ifndef LIB_GAMEIO_H_
#define LIB_GAMEIO_H_

#include "unolib.h"

typedef struct {
	unsigned int size, max;
	Card *cards;
}*Hand;

typedef struct Turnorder {
	unsigned int size;
	Hand *players;
}*Turnorder;

Hand new_hand(unsigned int size);
Card remove_card(Hand hand, unsigned int index);
void add_cards(Hand hand, unsigned int count, Card cards[]);
void free_hand(Hand hand);
Turnorder new_turnorder(unsigned int size);
void free_turnorder(Turnorder turnorder);

#endif /* LIB_GAMEIO_H_ */
